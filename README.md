# Краткое описание проекта

## Структура проекта

main
* [StorageEntity.java](src/main/java/storage/service/entity/StorageEntity.java) – основная сущность хранения данных (см. соответствующий пункт ниже)
* [StorageServiceController.java](src/main/java/storage/service/controller/StorageServiceController.java) – имплементация REST методов
* [StorageServiceRepository.java](src/main/java/storage/service/repository/StorageServiceRepository.java) – основной класс с реализацией
* [StorageServiceService.java](src/main/java/storage/service/service/StorageServiceService.java) – интерфейс методов
	* [StorageServiceImpl.java](src/main/java/storage/service/service/imp/StorageServiceImpl.java) – имплементация методов
* [StorageServiceApplication.java](src/main/java/storage/service/StorageServiceApplication.java) – конфигурация и запуск приложения
resources
* [application.properties](src/main/resources/application.properties) – файл с настройками сервиса
* [data.sql](src/main/resources/data.sql) – создание и инициализация H2 базы данных (см. соответствующий пункт ниже)

test
* [StorageServiceTest.java](src/test/java/storage/service/StorageServiceTest.java) – модульные тесты
resources – необходимые ресурсы для модульных тестов

## Структура основной сущности

StorageEntity
* Ключ записи – **id** – Long
* Данные записи – **value** – String
* Время создания записи – **createdAt** – String
* Время удаления записи – **deletedAt** – String
* Продолжительность жизни записи в секундах – **ttl** – Long

## H2

H2 – легковесная база данных Java с открытым исходным кодом. База данных работает в качестве базы данных памяти, что означает, что данные не будут сохраняться на диске.

'Создание базы данных:'

```SQL
DROP TABLE IF EXISTS storage;

CREATE TABLE storage (
  id BIGINT PRIMARY KEY,
  value VARCHAR(250) NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP DEFAULT NULL,
  ttl BIGINT DEFAULT NULL
  );
```

'Инициализация базы данных:'

```SQL
INSERT INTO storage (id, value, ttl) VALUES 
  (1, 'Dangote', 200), 
  (2, 'Gates', 200), 
  (3, 'Alakija', 200);|
```

## Перечисление REST-методов StorageService

1. **GET** /req/value/{id}
* Вход: id – идентификационный номер записи в таблице
* Выход: возвращение данных, которые хранятся по переданному id
2. **POST** /req/value
* Пример запроса:
```JavaScript
{
    "id": 5,
    "value": "ui",
    "ttl": 200
}
```
* Выход: метка успешности или неуспешности операции
* Тело сообщения:

| Поле  | Описание                                    | Тип    | Обязательность |
|-------|---------------------------------------------|--------|----------------|
| id    | Идентификационный номер                     | Long   | Да             |
| value | Содержание                                  | String | Да             |
| ttl   | Продолжительность жизни записи (в секундах) | Long   | Нет            |

3. **DEL** /req/value/{id}
* Вход: id – идентификационный номер записи в таблице
* Выход: возвращение данных, которые хранились по переданному id
4. **GET** /req/dump
* Вход: –
* Выход: файл с сохранённым состоянием хранилища
5. **POST** /req/dump
* Вход: файл с сохранённым состоянием хранилища
* Выход: метка об успешности или неуспешности операции

## Инструкция по сборке

1. Перед сборкой необходимо открыть файл [application.properties](src/main/resources/application.properties) для настройки сервиса
2. Сборка осуществляется с использованием Maven через консоль:
```JavaScript
mvn clean install
```
3. Для запуска приложения необходимо зайти в папку …\storage_service\target\dist и запустить следующую команду через консоль, дополнительно указав путь к файлу application.ptoperties
```JavaScript
java -jar storageservicejava-0.0.1-SNAPSHOT.jar --spring.config.location=PATH//application.properties
```
