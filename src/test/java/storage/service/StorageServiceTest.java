package storage.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import storage.service.entity.StorageEntity;
import storage.service.repository.StorageServiceRepository;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@SpringBootTest
public class StorageServiceTest {
    private static final Long FIRST_ID = 1L;
    private static final Long SECOND_ID = 2L;
    private static final Long THIRD_ID = 3L;
    private static final Long FALSE_ID = 50L;
    private static final Long YA_ID = 7L;
    private static final Long CORRUPTED_ID = 0L;

    private static final String FIRST_VALUE = "Dangote";
    private static final String THIRD_VALUE = "Alakija";
    private static final String YA_VALUE = "New Dangote";
    private static final String CORRUPTED_VALUE = null;

    private static final Long TTL = 200L;

    private static final String POST_NEW_MESSAGE = "Successfully inserted note with id: ";
    private static final String POST_UPDATE_MESSAGE = "Successfully updated note with id: ";
    private static final String CORRUPT_ID_MESSAGE = "Id field is corrupt.";
    private static final String CORRUPT_VALUE_MESSAGE = "Value field is corrupt.";
    private static final String LOAD_SUCCESS = "Successfully loaded dump file!";
    private static final String LOAD_ERROR = "Error while reading data!";
    private static final String LOAD_CORRUPTED = "Sorry about that...";

    @Autowired
    private StorageServiceRepository storageServiceRepository;

    //GET
    @Test
    void givenIdForGetAndReturnStorageEntity() {
        StorageEntity expectedResult = new StorageEntity();
        expectedResult.setId(FIRST_ID);
        expectedResult.setValue(FIRST_VALUE);
        expectedResult.setTtl(TTL);

        StorageEntity actualResult = storageServiceRepository.getStorageEntity(FIRST_ID);
        Assertions.assertEquals(expectedResult.getId(), actualResult.getId());
        Assertions.assertEquals(expectedResult.getValue(), actualResult.getValue());
        Assertions.assertEquals(expectedResult.getTtl(), actualResult.getTtl());
    }

    @Test
    void givenFalseIdForGetAndReturnStorageEntity() {
        StorageEntity actualResult = storageServiceRepository.getStorageEntity(FALSE_ID);
        Assertions.assertNull(actualResult);
    }

    //POST - new
    @Test
    void givenNewStorageEntityForPostAndReturnString() {
        StorageEntity storageEntity = new StorageEntity();
        storageEntity.setId(YA_ID);
        storageEntity.setValue(FIRST_VALUE);
        storageEntity.setTtl(TTL);

        String actualResult = storageServiceRepository.setStorageEntity(storageEntity);
        Assertions.assertEquals(POST_NEW_MESSAGE + storageEntity.getId().toString(), actualResult);
    }

    @Test
    void givenStorageEntityForPostAndReturnString() {
        StorageEntity storageEntity = new StorageEntity();
        storageEntity.setId(SECOND_ID);
        storageEntity.setValue(YA_VALUE);
        storageEntity.setTtl(TTL);

        String actualResult = storageServiceRepository.setStorageEntity(storageEntity);
        Assertions.assertEquals(POST_UPDATE_MESSAGE + storageEntity.getId().toString(), actualResult);
    }

    @Test
    void givenCorruptedIdInStorageEntityForPostAndReturnString() {
        StorageEntity storageEntity = new StorageEntity();
        storageEntity.setId(CORRUPTED_ID);
        storageEntity.setValue(YA_VALUE);
        storageEntity.setTtl(TTL);

        String actualResult = storageServiceRepository.setStorageEntity(storageEntity);
        Assertions.assertEquals(CORRUPT_ID_MESSAGE, actualResult);
    }

    @Test
    void givenCorruptedValueInStorageEntityForPostAndReturnString() {
        StorageEntity storageEntity = new StorageEntity();
        storageEntity.setId(FIRST_ID);
        storageEntity.setValue(CORRUPTED_VALUE);
        storageEntity.setTtl(TTL);

        String actualResult = storageServiceRepository.setStorageEntity(storageEntity);
        Assertions.assertEquals(CORRUPT_VALUE_MESSAGE, actualResult);
    }

    //DELETE
    @Test
    void givenIdForDeleteAndReturnStorageEntity() {
        StorageEntity expectedResult = new StorageEntity();
        expectedResult.setId(THIRD_ID);
        expectedResult.setValue(THIRD_VALUE);
        expectedResult.setTtl(TTL);

        StorageEntity actualResult = storageServiceRepository.removeStorageEntity(THIRD_ID);
        Assertions.assertEquals(expectedResult.getId(), actualResult.getId());
        Assertions.assertEquals(expectedResult.getValue(), actualResult.getValue());
        Assertions.assertEquals(expectedResult.getTtl(), actualResult.getTtl());
    }

    @Test
    void givenFalseIdForDeleteAndReturnStorageEntity() {
        StorageEntity actualResult = storageServiceRepository.removeStorageEntity(FALSE_ID);
        Assertions.assertNull(actualResult);
    }

    //DUMP
    @Test
    void getDumpAndReturnFile() {
        File actualResult = storageServiceRepository.dumpStorageEntity();
        Assertions.assertNotNull(actualResult);
    }

    //LOAD DUMP
    @Test
    void givenFileForLoadAndReturnText() throws Exception {
        String path = "src/test/resources/dump.json";
        File file = new File(path);

        try {
            MultipartFile multipartFile = new MockMultipartFile("dump.json", new FileInputStream(file));
            String actualResult = storageServiceRepository.loadStorageEntity(multipartFile);
            Assertions.assertEquals(LOAD_SUCCESS, actualResult);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void givenCorruptedFileForLoadAndReturnText() {
        String path = "src/test/resources/corrupted_dump.json";
        File file = new File(path);

        try {
            MultipartFile multipartFile = new MockMultipartFile("dump.json", new FileInputStream(file));
            String actualResult = storageServiceRepository.loadStorageEntity(multipartFile);
            Assertions.assertEquals(LOAD_ERROR, actualResult);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void givenBadFileForLoadAndReturnText() {
        String path = "src/test/resources/corrupted.docx";
        File file = new File(path);

        try {
            MultipartFile multipartFile = new MockMultipartFile("dump.json", new FileInputStream(file));
            String actualResult = storageServiceRepository.loadStorageEntity(multipartFile);
            Assertions.assertEquals(LOAD_CORRUPTED, actualResult);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
