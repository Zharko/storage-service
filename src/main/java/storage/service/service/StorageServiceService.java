package storage.service.service;

import org.springframework.web.multipart.MultipartFile;
import storage.service.entity.StorageEntity;

import java.io.File;

public interface StorageServiceService {

    StorageEntity getValue(Long id);

    String setValue(StorageEntity storageEntity);

    File dumpValue();

    String loadValue(MultipartFile file);

    StorageEntity removeValue(Long id);
}
