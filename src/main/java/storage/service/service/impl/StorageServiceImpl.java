package storage.service.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import storage.service.entity.StorageEntity;
import storage.service.repository.StorageServiceRepository;
import storage.service.service.StorageServiceService;

import java.io.File;

@Service
@Slf4j
public class StorageServiceImpl implements StorageServiceService {

    private final StorageServiceRepository storageServiceRepository;

    public StorageServiceImpl(StorageServiceRepository storageServiceRepository) {
        this.storageServiceRepository = storageServiceRepository;
    }

    @Override
    public StorageEntity getValue(Long id) {
        return storageServiceRepository.getStorageEntity(id);
    }

    @Override
    public String setValue(StorageEntity storageEntity) {
        return storageServiceRepository.setStorageEntity(storageEntity);
    }

    @Override
    public File dumpValue() {
        return storageServiceRepository.dumpStorageEntity();
    }

    @Override
    public String loadValue(MultipartFile file) {
        return storageServiceRepository.loadStorageEntity(file);
    }

    @Override
    public StorageEntity removeValue(Long id) {
        return storageServiceRepository.removeStorageEntity(id);
    }
}
