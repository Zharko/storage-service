package storage.service.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;
import storage.service.entity.StorageEntity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Repository
@Slf4j
public class StorageServiceRepository {
    @Value("${default.ttl}")
    private Long defaultTtl;
    @Value("${path.file}")
    private String filePath;
    @Value("${path.file.name}")
    private String fileName;

    private final JdbcTemplate jdbcTemplate;

    public StorageServiceRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private void insertIntoTable(StorageEntity storageEntity) {
        String query = "INSERT INTO storage (id, value, ttl) VALUES (?, ?, COALESCE(?," + defaultTtl + "))";
        jdbcTemplate.update(query, storageEntity.getId(),
                storageEntity.getValue(), storageEntity.getTtl());
    }

    private void cleanTable() {
        String query = "TRUNCATE TABLE storage";
        jdbcTemplate.execute(query);
    }

    private String validateSet(StorageEntity storageEntity) {
        if (storageEntity.getId() == 0) {
            return "Id field is corrupt.";
        } else if (storageEntity.getValue() == null || storageEntity.getValue().isEmpty()) {
            return "Value field is corrupt.";
        }
        return null;
    }

    public StorageEntity getStorageEntity(Long id) {
        try {
            String query = "SELECT id, value, ttl FROM STORAGE\n" +
                    "WHERE id = ? AND deleted_at is null";
            return jdbcTemplate.queryForObject(query,
                    (resultSet, s) -> new StorageEntity(resultSet.getLong("id"),
                            resultSet.getString("value"),
                            null,
                            null,
                            resultSet.getLong("ttl")),
                    id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (Exception e) {
            log.error("Something terrible happened!", e);
            return null;
        }
    }

    public String setStorageEntity(StorageEntity storageEntity) {
        String validationRes = validateSet(storageEntity);
        if (validationRes != null) {
            return validationRes;
        }

        StorageEntity checkStorageEntity = getStorageEntity(storageEntity.getId());
        try {
            if (checkStorageEntity != null) {
                String updateQuery = "UPDATE STORAGE SET id = ?, value = ?, ttl = COALESCE(?," + defaultTtl + ")\n" +
                        "WHERE id = ?";
                jdbcTemplate.update(updateQuery, storageEntity.getId(),
                        storageEntity.getValue(), storageEntity.getTtl(),
                        storageEntity.getId());
                return "Successfully updated note with id: " + storageEntity.getId();
            } else {
                insertIntoTable(storageEntity);
                return "Successfully inserted note with id: " + storageEntity.getId();
            }
        } catch (Exception e) {
            log.error("Exception!", e);
            return "Exception!";
        }
    }

    public StorageEntity removeStorageEntity(Long id) {
        try {
            String query = "UPDATE STORAGE SET deleted_at = CURRENT_TIMESTAMP()\n" +
                    "WHERE id = ?";
            StorageEntity storageEntity = getStorageEntity(id);
            jdbcTemplate.update(query, id);

            return storageEntity;
        } catch (Exception e) {
            log.error("Something terrible happened!", e);
            return null;
        }
    }

    public File dumpStorageEntity() {
        String query = "SELECT id, value, ttl FROM STORAGE\n" +
                "WHERE deleted_at is null";

        try {
            List<StorageEntity> storageEntityList = jdbcTemplate.query(query,
                    (resultSet, s) -> new StorageEntity(resultSet.getLong("id"),
                            resultSet.getString("value"),
                            null,
                            null,
                            resultSet.getLong("ttl")));

            File output = new File(filePath + fileName);
            if (!output.exists()) {
                Files.createFile(Paths.get(filePath + fileName));
            }

            try (BufferedWriter writer = new BufferedWriter(new FileWriter(output))) {
                ObjectMapper objectMapper = new ObjectMapper();

                storageEntityList.forEach(storageEntity -> {
                    try {
                        String json = objectMapper.writeValueAsString(storageEntity);
                        writer.write(json);
                        writer.newLine();
                    } catch (IOException e) {
                        log.error("Something bad happened when writing into file!", e);
                    }
                });

            }
            return output;
        } catch (Exception e) {
            log.error("Something even horrifying happened!", e);
            return null;
        }
    }

    public String loadStorageEntity(MultipartFile multipartFile) {
        try {
            File file = new File(filePath + "1" + multipartFile.getOriginalFilename());

            try (OutputStream os = new FileOutputStream(file)) {
                os.write(multipartFile.getBytes());
            } catch (Exception e) {
                log.error("Some other exception!", e);
                return "File corrupted!";
            }

            List<String> stringList = Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
            ObjectMapper objectMapper = new ObjectMapper();
            List<StorageEntity> storageEntityList = new ArrayList<>();

            for (String str : stringList) {
                try {
                    storageEntityList.add(objectMapper.readValue(str, StorageEntity.class));
                } catch (JsonProcessingException e) {
                    log.error("Error while reading data!", e);
                    return "Error while reading data!";
                }
            }
			
			file.deleteOnExit();
            cleanTable();
            storageEntityList.forEach(this::insertIntoTable);
            return "Successfully loaded dump file!";

        } catch (Exception e) {
            log.error("Something bad happened!", e);
            return "Sorry about that...";
        }
    }
}
