package storage.service.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import storage.service.entity.StorageEntity;
import storage.service.service.StorageServiceService;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.file.Files;
import java.util.Optional;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/req")
@Slf4j
public class StorageServiceController {
    private final StorageServiceService storageServiceService;

    public StorageServiceController(StorageServiceService storageServiceService) {
        this.storageServiceService = storageServiceService;
    }

    @GetMapping(value = "/value/{id}")
    public Callable<ResponseEntity<StorageEntity>> getValue(@PathVariable Long id) {
        return () -> {
            try {
                return ResponseEntity.status(200).body(storageServiceService.getValue(id));
            } catch (Exception e) {
                return ResponseEntity.badRequest().build();
            }
        };
    }

    @PostMapping(value = "/value")
    public Callable<ResponseEntity<String>> setValue(@RequestBody StorageEntity storageEntity) {
        return () -> {
            try {
                return ResponseEntity.status(200).body(storageServiceService.setValue(storageEntity));
            } catch (Exception e) {
                return ResponseEntity.badRequest().build();
            }
        };
    }

    @DeleteMapping(value = "/value/{id}")
    public Callable<ResponseEntity<StorageEntity>> removeValue(@PathVariable Long id) {
        return () -> {
            try {
                return ResponseEntity.status(200).body(storageServiceService.removeValue(id));
            } catch (Exception e) {
                return ResponseEntity.badRequest().build();
            }
        };
    }

    @GetMapping(value = "/dump")
    public Callable<ResponseEntity<Resource>> dumpValue() {
        return () -> {
            try {
                return generateJsonResponse(storageServiceService.dumpValue(), "dump");
            } catch (Exception e) {
                return ResponseEntity.badRequest().build();
            }
        };
    }

    @PostMapping(value = "/dump")
    public Callable<ResponseEntity<String>> loadValue(@RequestBody MultipartFile file) {
        return () -> {
            try {
                return ResponseEntity.status(200).body(storageServiceService.loadValue(file));
            } catch (Exception e) {
                return ResponseEntity.badRequest().build();
            }
        };
    }

    private HttpHeaders getHeaders(String fileName) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
        headers.add("file_name", fileName);
        headers.add("Access-Control-Allow-Headers", "*");
        headers.add("Access-Control-Expose-Headers", "*");
        return headers;
    }

    private ResponseEntity<Resource> generateJsonResponse(File output, String nameExport) {
        try {
            Optional<byte[]> optionalBytes = Optional.of(Files.readAllBytes(output.toPath()));
            byte[] file = optionalBytes.get();
            try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(file)) {
                InputStreamResource resource = new InputStreamResource(byteArrayInputStream);
                String fileName = nameExport + ".json";
                return ResponseEntity.ok()
                        .headers(getHeaders(fileName))
                        .contentLength(file.length)
                        .contentType(MediaType.parseMediaType("application/octet-stream"))
                        .body(resource);
            } catch (Exception e) {
                log.error("Error saving file", e);
                return ResponseEntity.badRequest().build();
            }
        } catch (Exception e) {
            log.error("Error convert file to byte array", e);
            return ResponseEntity.badRequest().build();
        }
    }

}
