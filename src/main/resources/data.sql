DROP TABLE IF EXISTS storage;

CREATE TABLE storage (
  id BIGINT PRIMARY KEY,
  value VARCHAR(250) NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP DEFAULT NULL,
  ttl BIGINT DEFAULT NULL
);

INSERT INTO storage (id, value, ttl) VALUES
  (1, 'Dangote', 200),
  (2, 'Gates', 200),
  (3, 'Alakija', 200);